/**
 * @file   Population.h
 * @Author Erik Ström
 * @date   October 2017
 * @version 0.1
*/
#ifndef POPULATION_H
#define POPULATION_H

#include<map>
#include<string>
#include "Cell.h"
#include "Support/Globals.h"
#include "GoL_Rules/RuleOfExistence.h"
#include "GoL_Rules/RuleFactory.h"

using namespace std;

/**
* @brief  Population class file
*
* @details
* The Population contains all the cells used for the program.
* It makes the cells on creation and set what rules to be used.
*
* Representation of the complete society of cell existance and interactions.
* The Population constitutes of all current, previous and future generations of cells, both living and dead
* as well as those not yet born. By mapping all cells to their respective positions in the simulation world,
* Population has the complete knowledge of each cell's whereabouts. Furthermore, the class is responsible for
* determining which rules should be required from the RuleFactory, and store the pointer to these as members.
* Population's main responsibility during execution is determining which rule to apply for each new generation
* and updating the cells to their new states.
*
* @test
* The correct amount of cells are need to be created.
* Test that there is a difference between the even and odd rules.
* Random created populations should be truly random and not repeated.
*/
class Population
{
private:
    int generation;
    map<Point, Cell> cells;
    RuleOfExistence* evenRuleOfExistence;
    RuleOfExistence* oddRuleOfExistence;

    void randomizeCellCulture();

    void buildCellCultureFromFile();

public:
    /**
     * @brief  Population constructor
     *
     * @details
     * Constructs an empty population without any arguments.
     *
     * @param VOID
     */
    Population() : generation(0), evenRuleOfExistence(nullptr), oddRuleOfExistence(nullptr) {}

    /**
     * @brief  Population destructor
     *
     * @details
     * Destructor that frees allocated memory. Including the rule of existence pointers.
     *
     * @param VOID
     */
    ~Population();

    /**
     * @brief  Population initiator
     *
     * @details
     * Initializing cell culture and the concrete rules to be used in simulation.
     * Rule arguments are strings. First is mandatory second is optional.
     * If no second rule is used the first will be used as both.
     *
     * @param evenRuleName STRING
     * @param oddRuleName = "" STRING
     */
    void initiatePopulation(string evenRuleName, string oddRuleName = "");

    /**
     * @brief  Calculate the cells for next generation
     *
     * @details
     * Update the cell population and determine next generational changes based on rules.
     * Increment and return the generation counter.
     *
     * @param VOID
     * @return INT. Returns the number of current generation.
     *
     */
    int calculateNewGeneration();

    /**
     * @brief  get cell from position
     *
     * @details
     * Returns reference to the cell from position.
     * Uses a point to locate the cell.
     * Throws runtime_error if population not initiated.
     *
     * @param position POINT. The position of cell you looking for.
     * @return Cell&. Reference to the asked cell.
     */
    Cell& getCellAtPosition(Point position) { return cells.at(position); }

    /**
     * @brief  Get population size
     *
     * @details
     * Returns the int of total ammount of cells in the population.
     *
     * @param VOID
     * @return INT. Total cell count.
     */
    int getTotalCellPopulation() { return cells.size(); }

};

#endif