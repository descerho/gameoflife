/**
 * @file   Cell.h
 * @Author Erik Ström
 * @date   October 2017
 * @version 0.1
*/
#ifndef cellH
#define cellH

#include "../../terminal/terminal.h"

// Data structure holding colors to visualize the state of cells.
struct StateColors {
    COLOR LIVING, // Representing living cell
            DEAD, // Representing dead cell
            OLD,  // Representing old cell
            ELDER;// Representing very old cell
}	/* Initiate default values. */
const STATE_COLORS = { COLOR::WHITE, COLOR::BLACK, COLOR::CYAN, COLOR::MAGENTA };

// Cell action. Determined by rule, and sent to cell for future change.
enum ACTION { KILL_CELL, IGNORE_CELL, GIVE_CELL_LIFE, DO_NOTHING };


/**
 * @brief  Cell class file
 *
 * @details
 * The Cell class is used for all cells inside the population.
 * It stores the value of the current and next state before changing.
 *
 * Cells represents a certain combination of row and column of the simulated world.
 * Cells may be of two types; rim cells, those representing the outer limits of the world,
 * or non-rim cells. The first cell type are immutable, exempt from the game's rules, and
 * thus their values may not be changed. The latter type, however, may be changed and edited
 * in ways specified by the rules.
 *
 * @test
 * The class need to be tested so it works as expected when creating cells and changing their value.
 */
class Cell {

private:
    struct CellDetails {	// encapsulate cell details
        int age;
        COLOR color;
        bool rimCell;
        char value;
    } details;

    struct NextUpdate {		// encapsulate changes to next state
        ACTION nextGenerationAction;
        COLOR nextColor;
        char nextValue;
        bool willBeAlive;	// some rules may need to know beforehand whether the cell will be alive
    } nextUpdate;

    void incrementAge() { details.age++; }

    void killCell() { details.age = 0; }

    void setCellValue(char value) { details.value = value; }

    void setColor(COLOR color) { this->details.color = color; }

public:
    /**
     * @brief  Cell constructor
     *
     * @details
     *  Creates empty cell, parametrars not needed.
     *
     * @param isRimCell = false, BOOL. If the cell is on the edge or not.
     * @param action = DO_NOTHING, ACTION . What the first generation of the cell will do.
    */
    Cell(bool isRimCell = false, ACTION action = DO_NOTHING);

    /**
     * @brief  Checks if Cell is alive
     *
     * @details
     * Bool that returns if the cell is alive or not
     * @param VOID
     * @return BOOL. If the cell is alive.
     *
     * @test
     * Dead cell should return false. Alive cell should return true.
     * Cells on the rim should always be dead
     */
    bool isAlive();

    /**
     * @brief  Update cells next action
     *
     * @details
     * Sets the cells next action to take in its coming update.
     * Unless Cell is at the rim or the current action is to give_cell_life and its alive.
     *
     * @param action ACTION. What action the cell will have in next generation.
     *
     * @test
     * Cells on the rim should not change.
     * Use setNextGenerationAction(). This should then return same value.
     */
    void setNextGenerationAction(ACTION action);

    /**
     * @brief  Cell update state
     *
     * @details
     * Updates the cell to its new state, based on stored update values.
     *
     * @param VOID
     *
     * @test
     * The cell should not have an updated current state corresponding to the old next action.
     * Cells new update state should be to do nothing.
     */
    void updateState();

    /**
     * @brief  Returns age of cell
     *
     * @details
     * Returns an int for the cell age. Age 0 indicates dead cell.
     *
     * @param VOID
     * @return INT. The age of the cell.
     *
     * @test
     * New dead cell should have age 0.
     * Alive cell that aged one generation should have 1.
    */
    int getAge() { return details.age; }

    /**
     * @brief  Return the color of cell
     *
     * @details
     * Returns the color value in form of COLOR
     *
     * @param VOID
     * @return COLOR. Current color of the cell.
     *
     * @test
     * Set color with setNextColor. Age one generation. This should return same value.
    */
    COLOR getColor() { return details.color; }

    /**
     * @brief  Returns if cell is on the rim
     *
     * @details
     * Determines whether the cell is a rim cell, and thus should be immutable.
     *
     * @param VOID
     * @return BOOL. True if the cell is located on the rim.
     *
     * @test
     * Make 3v3 grid. Middle cell should be negative. Others positive
    */
    bool isRimCell() { return details.rimCell; }

    /**
     * @brief  Sets color used after next update
     *
     * @details
     * Sets the color the cell will have after its next update.
     *
     * @param nextColor COLOR. Set what color cell should have after next generation.
     *
     * @test
     * Set color with setNextColor. Age one generation. Check results of getColor().
     * Tested with getColor()
    */
    void setNextColor(COLOR nextColor) { this->nextUpdate.nextColor = nextColor; }

    /**
     * @brief  Returns Cells value
     *
     * @details
     * Returns the value the cell uses for printing.
     *
     * @param VOID
     * @return CHAR. The character the cell is using as output
     *
     * @test
     * SetNextCellValue on cell. Do one generation. Use getCellValue. Should return the same val.
    */
    char getCellValue() { return details.value; }

    /**
     * @brief  Set next print value
     *
     * @details
     * Sets the next character value of the cell, which will be printed to screen.
     *
     * @param value CHAR. Set character of next generation for cell.
     *
     * @test
     * SetNextCellValue on cell. Do one generation. Use getCellValue. Should return the same val.
     * Tested with getCellValue.
    */
    void setNextCellValue(char value) { nextUpdate.nextValue = value; }

    /**
     * @brief  Set if alive next generation
     *
     * @details
     * WARNING, this is not implemented in the code and is not used!
     * Sets whether the cell is alive/dead next generation.
     * Used for some RuleOfExistence that need to know beforehand.
     *
     * To implement this code a check need to be run after applying RuleOfExistence on current generation
     * and before starting next generation. Similar check as Cell:UpdateState(), but without changing other cells values.
     *
     * @param isAliveNext BOOL. Sets if the cell will be alive in next generation.
     *
     * @test
     * Set to false. Check with isAliveNext() that its still false. Do same with true.
    */
    void setIsAliveNext(bool isAliveNext) { nextUpdate.willBeAlive = isAliveNext; }

    /**
     * @brief  Check if its alive next generation
     *
     * @details
     * WARNING, this is not implemented in the code, this value will not reflect if its alive next generation!
     * Check whether the cell is alive/dead next generation.
     * Used for some RuleOfExistence that need to know beforehand.
     *
     * @param VOID
     * @return BOOL. Return if the cell will be alive in next generation
     *
     * @test
     * Tested together with setIsAliveNext().
    */
    bool isAliveNext() { return nextUpdate.willBeAlive; }

    /**
     * @brief  Get action
     *
     * @details
     * Gets the cells next action.
     *
     * @param VOID
     * @return ACTION&. Returns the action the cell will have next generation.
     *
     * @test
     * Tester together with setNextGenerationAction().
    */
    ACTION& getNextGenerationAction() { return nextUpdate.nextGenerationAction; }

};

#endif
