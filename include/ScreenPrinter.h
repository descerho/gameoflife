/*
 * Filename    ScreenPrinter.h
 * Author      Erik Ström
 * Date        October 2017
 * Version     0.1
*/



#ifndef screenPrinterH
#define screenPrinterH


#include "../terminal/terminal.h"
#include "Cell_Culture/Population.h"

/**
 * @brief Responsible for presenting the simulation on screen.
 * @details Controls a Terminal-object which is used to present the simulation on screen.
 * There is only one instance allowed which is controlled by a "singleton pattern" i.e. the constructor is private and the only way to get an instance of the class is to use the static function getInstance which returns a reference to the only object.
 */
class ScreenPrinter {

private:
    Terminal terminal;

    ScreenPrinter() {}

public:
    /**
     * @brief singleton pattern
     * @details this design pattern assures that only a single object is created.
     * @return a static reference to the only instance of this class.
     * @test try to get the reference to the instance a couple of time and see if they reference the same object
     */
    static ScreenPrinter& getInstance() {
        static ScreenPrinter instance;
        return instance;
    }


    /**
     * @brief Prints the representation of the supplied population on screen.
     * @details Loops through and prints the Cell -value in the appropriate color.
     * @param population the population to be printed on screen
     * @test a visual inspection is the only thing that comes to mind. Test for distortions in presentation by using different sized grids with easy-to-predict patterns.
     */
    void printBoard(Population& population);

    /**
     * @brief Prints the arguments-help screen
     * @details Lists all the available arguments for running the program. Also lists options and/or defaults where it is appropriate.
     * @test Visual test. Does not depend on the current state, so just call the method once.
     */
    void printHelpScreen();

    /**
     * @brief Prints the supplied message on screen
     * @param message the message to be printed.
     * @test Visual test. Does not depend on the current state, just call it once.
     */
    void printMessage(string message);

    /**
     * @brief Clears the screen
     * @test Visual test. Terminal does the clearing. Does not depend on this objects state, just call it once.
     */
    void clearScreen();
};

#endif
