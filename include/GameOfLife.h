/*
 * Filename    GameOfLife.h
 * Author      Erik Ström
 * Date        October 2017
 * Version     0.1
*/

#ifndef GameOfLifeH
#define GameOfLifeH

#include "Cell_Culture/Population.h"
#include "ScreenPrinter.h"
#include <string>

/**
 * @brief The hub of the simulation
 * @details Creates and manages Population and the ScreenPrinter instances. Will instruct the Population of cells to keep
 * updating as long as the specified number of generations is not reached, and for each iteration instruct
 * ScreenPrinter to write the information on screen.
*/
class GameOfLife {

private:
    Population population;
    ScreenPrinter& screenPrinter;
    int nrOfGenerations;

public:

    /**
     * @brief Constructor
     * @param nrOfGenerations the maximum generation to simulate to. The initial generation is generation 1.
     * @param evenRuleName the name of the rule to apply on even-numbered generations.
     * @param oddRuleName  the name of the rule to apply on odd-numbered generations. 
     * @test Test the constructor with a few different parameter-variations. It is not designed to throw, so feel free to test unexpected parameters. 
     */
    GameOfLife(int nrOfGenerations, string evenRuleName, string oddRuleName);

    /**
     * @brief Clears the screen and runs the simulation
     * @test Run the simulation from a few different parameter-variations. 
     * This is hard to do in a program, a visual test could be appropriate.
     */
    void runSimulation();

};

#endif
