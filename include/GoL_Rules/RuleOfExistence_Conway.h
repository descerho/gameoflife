/*
 * Filename    RuleOfExistence_Conway.h
 * Author      Erik Ström
 * Date        October 2017
 * Version     0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
#define GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H

#include "RuleOfExistence.h"
/**
 * @brief Conway's RuleOfExistence, applying actions based on PopulationLimits on all 8 surrounding neighbours.
 * @details Concrete RuleOfExistence, implementing Conway's rule of determining alive neighbours surrounding the cell
 * by checking all 8 directions, 4 x Cardinal + 4 x Diagonal. PopulationLimits are set as;
 * 
 * - UNDERPOPULATION	< 2	**Cell dies of loneliness**
 * - OVERPOPULATION		> 3	**Cell dies of overcrowding**
 * - RESURRECTION		= 3	**Cell is infused with life**
 * @test Test the result by using the manually calculated expected results test/test-poplation-initial-state.txt
 * and test/test-population-conway.txt
 */
class RuleOfExistence_Conway : public RuleOfExistence
{
private:

public:
    /**
     * @brief Consructor
     * @details Sets the static rules for this rule-set using the RuleOfExistence constructor
     * @param cells a reference to the map of Cell :s upon which the rules will be applied.
     *
     * @test
     * Create with and without cells in map.
     */
    RuleOfExistence_Conway(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, ALL_DIRECTIONS, "conway") {}

    /**
     * @brief Destructor
     */
    ~RuleOfExistence_Conway() {}

    /**
     * @brief Executes the rule.
     * @details Calculates the action and corresponding color which will be set on every Cell
     * in the next update.
     * @test Import cells based on test file and age once. Compare to the manually calculated test-files.
     * The manually calculated file for conway should  be correct. Incorrect compared to von-neumann.
     */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
