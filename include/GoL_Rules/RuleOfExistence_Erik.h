/*
 * Filename    RuleOfExistence_Erik.h
 * Author      Erik Ström
 * Date        October 2017
 * Version     0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H
#define GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H

#include "RuleOfExistence.h"

/**
 * @brief Erik's RuleOfExistence, based on Conway's rule while also differentiate the appearance of cells based on their age.
 * 
 * @details Concrete Rule of existence, implementing Erik's rule.
 * Sentient lifeforms is rarely created, but once in a while a cell has lived enough generations to become as wise as Erik.
 * 
 * - Once a cell has survived a minimum amount of generations it will recieve a color to distinguish itself from younger ones.
 * - If such a cell would then survive another set amount of generations, it will be marked with a value of **E**.
 * - In the extreme case, where the cell has achieved above requirements and is determined to be the oldest living cell, it will
 * become a **prime elder**, and have its color changed once again. A generation may only have one such elder.
 * @test Test the result by using the manually calculated expected results test/test-poplation-initial-state.txt
 * and test/test-population-erik.txt
 * Test to see if color and value of cells update correctly.
 */
class RuleOfExistence_Erik : public RuleOfExistence
{
private:
    char usedCellValue;	// char value to differentiate very old cells.
    Cell* primeElder;

    void erikfyCell(Cell& cell, ACTION action);
    void setPrimeElder(Cell* newElder);

public:
    /**
     * @brief Constructor
     * @details Sets the static rules for this rule-set using the RuleOfExistence constructor
     * @param cells a reference to the map of Cell :s upon which the rules will be applied.
     */
    RuleOfExistence_Erik(map<Point, Cell>& cells)
            : RuleOfExistence({2,3,3}, cells, ALL_DIRECTIONS, "erik"), usedCellValue('E') {
        primeElder = nullptr;
    }

    /**
     * @brief Destructor
     */
    ~RuleOfExistence_Erik() {}

    /**
     * @brief Executes the rule.
     * @details Calculates the action, corresponding color and Cell value  which will be set on every Cell
     * in the next update.
     * @test By using the manually calculated test-files. And also running it in a few other variations.
     * The colors and cell values of the older cells should be tested.
     */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H
