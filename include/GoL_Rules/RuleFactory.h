/*
 * Filename    RuleFactory.h
 * Author      Erik Ström
 * Date        October 2017
 * Version     0.1
*/

#ifndef RULEFACTORY_H
#define RULEFACTORY_H

#include "GoL_Rules/RuleOfExistence.h"

/**
 * @brief Singleton class to handle creation of RulesOfExistence objects
 */
class RuleFactory
{
private:
    RuleFactory() {}

public:
    /**
     * @brief Singleton design pattern to get the only running instance of this class.
     * @returns A reference to the only running instance of this class.
     */
    static RuleFactory& getInstance();

    /**
     * @brief Creates and returns a RulesOfExistence object
     * @details The rule is distinguished by a string representing the name of the rule.
     * Defaults to conway if the supplied rule-name is unknown.
     * @param cells A reference to the map containing the Cell Population upon which the rules should act on.
     * @param ruleName The name of the rule-set to apply on the Cell Population. Defaults to conway if the
     * supplied name is unknown.
     * @returns A pointer to the created RuleOfExistence object
     *
     * @test
     * See that the correct rule get used by RuleOfExistence
     * Nothing bad should happen if string is invalid
     */
    RuleOfExistence* createAndReturnRule(map<Point, Cell>& cells, string ruleName = "conway");
};

#endif
