/**
 * @file    RuleOfExistence.h
 * @author      Erik Ström
*/
#ifndef RULEOFEXISTENCE_H
#define RULEOFEXISTENCE_H

#include<string>
#include<map>
#include<vector>
#include "Cell_Culture/Cell.h"
#include "Support/Globals.h"
using namespace std;

/*
These rules lies at the heart
of the simulation, and determines the fate of each cell in the world population.
*/

// Data structure for storing population limits. Used by rules to determine what ACTION to make.
struct PopulationLimits {
    int UNDERPOPULATION, // cell dies of loneliness
            OVERPOPULATION, // cell dies of over crowding
            RESURRECTION; // cell lives on / is resurrected
};

// Data structure for storing directional values. Used by rules to determine neighbouring cells.
struct Directions {
    int HORIZONTAL, VERTICAL;
};

// All directions; N, E, S, W, NE, SE, SW, NW
const vector<Directions> ALL_DIRECTIONS{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 },{ 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

// Cardinal directions; N, E, S, W
const vector<Directions> CARDINAL{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 } };

// Diagonal directions; NE, SE, SW, NW
const vector<Directions> DIAGONAL{ { 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

/*

*/
/**
 * @brief Abstract base class, upon which concrete rules will derive.
 * @details The derivations of RuleOfExistence is what determines the culture of Cell Population. 
 * Each rule implements specific behaviours and so may execute some parts in different orders. 
 * In order to accommodate this requirement RuleOfExistence will utilize a **Template Method** desing pattern, 
 * where all derived rules implements their logic based on the virtual method executeRule().
 */
class RuleOfExistence {
protected:
    string ruleName;

    /**
     * @brief Reference to the population of cells
     */
    map<Point, Cell>& cells;

    /** 
     * @brief Amounts of alive neighbouring cells, with specified limits
     */
    const PopulationLimits POPULATION_LIMITS;

    /**
     * @brief The directions, by which neighbouring cells are identified
     */
    const vector<Directions>& DIRECTIONS;

    /**
     * @brief Counts the number of alive neighbors from the supplied Point
     * @param currentPoint a Point representing the coordinates of the current cell.
     * @returns the number of alive neighbors.
     * @test Indirectly by executing rules
     */
    int countAliveNeighbours(Point currentPoint);

    /**
     * @brief Determines the action of a cell based on its current state.
     * @details The action is a function of alive neighbors and if the cell is alive.
     * @param aliveNeighbours the number of alive neighbors @see countAliveNeighbours
     * @param isAlive if the current cell is alive or not.
     * @return the rule-determined ACTION to take in the
     * current circumstance. @see Cell.h for ACTION documentation. 
     * @test Indirectly by executing rules.
     */
    ACTION getAction(int aliveNeighbours, bool isAlive);

public:
    /**
     * @brief Constructor
     * @param limits Sets the PopulationLimits
     * @param cells A reference to the map of Cell :s upon which the set of rule will act. 
     * @param DIRECTIONS A reference to a vector of Directions which represents the relative coordinates
     * of a Cell :s neighbors.
     * @param ruleName the name of the current rule.
     * @test Indirectly by creating derived objects.
     */
    RuleOfExistence(PopulationLimits limits, map<Point, Cell>& cells, const vector<Directions>& DIRECTIONS, string ruleName)
            : POPULATION_LIMITS(limits), cells(cells), DIRECTIONS(DIRECTIONS), ruleName(ruleName) {}

    /**
     * @brief Virtual destructor
     */
    virtual ~RuleOfExistence() {}

    /**
     * @brief Execute rule, in order specific to the concrete rule, by utilizing template method DP
     */
    virtual void executeRule() = 0;

    /**
     * @returns the name of the current rule.
     * @test by using derived objects.
     */
    string getRuleName() { return ruleName; }
};

#endif
