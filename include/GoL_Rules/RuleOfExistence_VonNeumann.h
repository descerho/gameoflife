/*
 * Filename    RuleOfExistence_VonNeumann.h
 * Author      Erik Ström
 * Date        October 2017
 * Version     0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
#define GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H

#include "RuleOfExistence.h"
/**
 * @brief Von Neumann's RuleOfExistence, differs from Conway in that only 4 neighbours are accounted for.
 * @details Concrete Rule of existence, implementing Von Neumann's rule.
 * Only difference from Conway is that neighbours are determined using only cardinal directions (N, E, S, W).
 * @test Test the result by using the manually calculated expected results test/test-poplation-initial-state.txt
 * and test/test-population-von_neumann.txt
 */
class RuleOfExistence_VonNeumann : public RuleOfExistence
{
private:

public:
    /**
     * @brief Consructor
     * @details Sets the static rules for this rule-set using the RuleOfExistence constructor
     * @param cells a reference to the map of Cell :s upon which the rules will be applied.
     */
    RuleOfExistence_VonNeumann(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, CARDINAL, "von_neumann") {}

    /**
     * @brief Destructor
     */
    ~RuleOfExistence_VonNeumann() {}

    /**
     * @brief Executes the rule.
     * @details Calculates the action and corresponding color which will be set on every Cell
     * in the next update.
     * @test By using the manually calculated test-files. And also running it in a few other variations.
     */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
