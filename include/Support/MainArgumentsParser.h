/**
 * @file   MainArgumentsParser.h
 * @Author Erik Ström
 * @date   October 2017
 * @version 0.1
*/
#ifndef mainArgumentsParserH
#define mainArgumentsParserH

#include "MainArguments.h"
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

/*

*/
/**
 * @brief  Static functions that parses the starting arguments
 *
 * @details
 * The program have different argument and flags before running.
 * This contains the static functions that parses the starting arguments for the application.
 * It only have one public function.
 *
 * @test
 * This should not fail even if something wrong is added as argument
 */
class MainArgumentsParser {
public:

    /**
     * @brief Runs the parser
     *
     * @details
     * This function will run and check for the argument added in the start string.
     * If they are found it will create mainArgument classes for each one.
     * For they that are missing he will instead add the default value
     *
     * @param argv [] char*. All characters added as argument when starting application
     * @param length int. The length of the argument chars.
     *
     * @test
     * Run different kind of inputs as argv and length. They should not make the program fail.
     *
    */
    ApplicationValues& runParser(char* argv[], int length);

private:
    ApplicationValues appValues;

    // Checks if a given option exists
    bool optionExists(char** begin, char** end, const std::string& option);

    // gets the given option value
    char* getOption(char** begin, char** end, const std::string& option);
};

#endif
