/**
 * @file   Globals.h
 * @Author Erik Ström
 * @date   October 2017
 * @version 0.1
 *
 * @brief  File for Global variables
 *
 * @details
 * Stores the global variables used by other classes.
 * Currently only used by two variables, the population size and filename for import.
 *
 */
#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include "SupportStructures.h"

using namespace std;

// The actual width and height of the used world
extern Dimensions WORLD_DIMENSIONS;

// Name of file to read
extern string fileName;


#endif