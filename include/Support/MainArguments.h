/**
 * @file   MainArguments.h
 * @Author Erik Ström
 * @date   October 2017
 * @version 0.1
*/
#ifndef GAMEOFLIFE_MAINARGUMENTS_H
#define GAMEOFLIFE_MAINARGUMENTS_H

#include "Globals.h"
#include "ScreenPrinter.h"
#include <sstream>
#include <string>

using namespace std;

struct ApplicationValues {
    bool runSimulation = true;
    string evenRuleName, oddRuleName;
    int maxGenerations = 100;

};
/**
 * @brief Class to handle the base arguments
 *
 * @details
 * Program runs with different switches.
 * All of the different arguments are passed into this class.
 * This is the base class used for other arguments
 *
 */
class BaseArgument {
protected:
    const string argValue;

    // inform the user that no value was provided for the argument
    void printNoValue();

public:

    /**
     * @brief Argument creator
     *
     * @details
     * Takes a string : argument value to create.
     * This is not doing anything, used for children classes only.
     *
     * @param argValue, STRING : argValue
     *
    */
    BaseArgument(string argValue) : argValue(argValue) {}

    /**
     * @brief Argument deconstruct
     *
     * @details
     * Virtual Deconstruct for the argument class.
     *
     * @param VOID
    */
    virtual ~BaseArgument() {}

    /**
     * @brief Execute the argument
     *
     * @details
     * Virutal, will not be used in this class
     * Changes the AppValues depending on the value of char.
     *
     * @param AppValues, ApplicationValues&. Ref to the collection of current values used program settings.
     * @param value, char*. Value used after the argument switch argument.
     *
    */
    virtual void execute(ApplicationValues& appValues, char* value = nullptr) = 0;

    /**
     * @brief  Returns the value of argument
     *
     * @details
     * Children classes set the argument when created.
     * This return that value.
     * Example, the helpArgument child will return "-h"
     *
     * @param VOID
     * @return STRING. Reference to the argument hold by this class.
     *
    */
    const string& getValue() { return argValue; }
};

/**
 * @brief  Class for argument for help screen
 *
 * @details
 * Child from BaseArgument class.
 * Handles the argument -h if it is entered.
 *
 * @test
 * Check that this argument can be created.
 * That correct argument value is returned.
 */
class HelpArgument : public BaseArgument {
public:

    /**
     * @brief Help Argument creator
     *
     * @details
     * The BaseArgument string need to be -h.
     * Creates an HelpArgument instance.
     *
     * @param BaseArgument("-h")
     *
    */
    HelpArgument() : BaseArgument("-h") {}

    /**
     * @brief Help Argument deconstruct
     *
     * @details
     * A normal deconstruct for the argument class.
     *
     * @param VOID
    */
    ~HelpArgument() {}

    /**
     * @brief Execute the help argument
     *
     * @details
     * This will show the help screen.
     * appValue will be set to not run project
     * Help argument don't use any value after switch, the "value" here is irrelevant
     *
     * @param AppValues, ApplicationValues&. Ref to the collection of current values used program settings.
     * @param value, char*. Value used after the argument switch argument
     *
     * @test
     * The program should not run when this switch is active
     * Nothing more happens if argument has a value
    */
    void execute(ApplicationValues& appValues, char* value);
};

/**
 * @brief  Amount of generations to simulate
 *
 * @details
 * Child from BaseArgument class.
 * Handles the argument -g if it is entered.
 * Take in an int after -g for how many generations / loops the program should do.
 *
 * @test
 * Check that this argument can be created.
 * That correct argument value is returned.
 * Test what happens if you try different input that the expected int
 */
class GenerationsArgument : public BaseArgument {
public:

    /**
     * @brief Generation Argument creator
     *
     * @details
     * The BaseArgument string need to be -g.
     * Creates an GenerationArgument instance.
     *
     * @param BaseArgument("-g")
     *
    */
    GenerationsArgument() : BaseArgument("-g") {}

    /**
     * @brief Generation Argument deconstruct
     *
     * @details
     * A normal deconstruct for the argument class.
     *
     * @param VOID
    */
    ~GenerationsArgument() {}

    /**
     * @brief Execute the Generation argument
     *
     * @details
     * This loads the generations and set it to max generations for the program.
     * Using a STOI conversion.
     * If the char is empty the program will not run.
     * Uses ScreenPrinter to make a message when that happens.
     *
     * @param AppValues, ApplicationValues&. Ref to the collection of current values used program settings.
     * @param value, char*. Value used after the argument switch argument
     *
     * @test
     * See if changes the appValue for max generations
     * If empty generations program wont start.
     * Test what happens if you try different input that the expected int
    */
    void execute(ApplicationValues& appValues, char* generations);
};

/**
 * @brief Custom population size
 *
 * @details
 * Child from BaseArgument class.
 * Handles the argument -s if it is entered.
 * Manage the size of a random population.
 * This is all overwritten if file import is used
 *
 * @test
 * Check that this argument can be created.
 * That correct argument value is returned.
 * The value here should be irrelevant together with -f switch
 */
class WorldsizeArgument : public BaseArgument {
public:

    /**
     * @brief Worldsize Argument creator
     *
     * @details
     * The BaseArgument string need to be -s.
     * Creates an WorldsizeArgument instance.
     *
     * @param BaseArgument("-s")
     *
    */
    WorldsizeArgument() : BaseArgument("-s") {}

    /**
     * @brief Worldsize Argument deconstruct
     *
     * @details
     * A normal deconstruct for the argument class.
     *
     * @param VOID
    */
    ~WorldsizeArgument() {}

    /**
     * @brief Execute the Worldsize argument
     *
     * @details
     * Change appValue for the size of population
     * Using istringstream to get data from dimensions adding them the the appValue for size.
     * If the dimensions is empty the program will not run.
     * Uses ScreenPrinter to make a message when that happens.
     *
     * @param AppValues, ApplicationValues&. Ref to the collection of current values used program settings.
     * @param value, char*. Value used after the argument switch argument
     *
     * @test
     * Check that dimensions change value of appValue for dimensions.
     * If empty dimensions program wont start.
     * Test what happens if you try different input that the expected "5X5" format
     * The dimensions value here should be irrelevant together with -f switch
    */
    void execute(ApplicationValues& appValues, char* dimensions);
};

/**
 * @brief  Initiate population from file
 *
 * @details
 * Child from BaseArgument class.
 * Handles the argument -f if it is entered.
 * The value after -f is the file name used to import population map
 *
 * @test
 * Check that this argument can be created.
 * That correct argument value is returned.
 */
class FileArgument : public BaseArgument {
public:

    /**
     * @brief File Argument creator
     *
     * @details
     * The BaseArgument string need to be "-f"
     * Creates an FileArgument instance.
     *
     * @param BaseArgument("-f")
     *
    */
    FileArgument() : BaseArgument("-f") {}

    /**
     * @brief File Argument deconstruct
     *
     * @details
     * A normal deconstruct for the argument class.
     *
     * @param VOID
    */
    ~FileArgument() {}

    /**
     * @brief Execute the File argument
     *
     * @details
     * If the fileNameArg is empty the program will not run.
     * Uses ScreenPrinter to make a message when that happens.
     * Note: This will not load the file, but add the file name to the global variable.
     *
     * @param AppValues, ApplicationValues&. Ref to the collection of current values used program settings.
     * @param value, char*. Value used after the argument switch argument
     *
     * @test
     * Check that global variable gets updated.
     * If empty fileNameArg program wont start.
     * Test file names with breaks in them. ("my file name.txt") etc
    */
    void execute(ApplicationValues& appValues, char* fileNameArg);
};

/**
 * @brief  Rule used for even generations
 *
 * @details
 * Child from BaseArgument class.
 * Handles the argument -er if it is entered.
 * Used to set what rule should be used for even generations
 *
 * @test
 * Check that this argument can be created.
 * That correct argument value is returned.
 */
class EvenRuleArgument : public BaseArgument {
public:

    /**
     * @brief EvenRule Argument creator
     *
     * @details
     * The BaseArgument string need to be -er.
     * Creates an EvenRule instance.
     *
     * @param BaseArgument("-er")
     *
    */
    EvenRuleArgument() : BaseArgument("-er") {}

    /**
     * @brief EvenRule Argument deconstruct
     *
     * @details
     * A normal deconstruct for the argument class.
     *
     * @param VOID
    */
    ~EvenRuleArgument() {}

    /**
     * @brief Execute the even rule argument
     *
     * @details
     * Sets the evenRule to the evenRuleName variable.
     * It dont check that is a valid rule name.
     * If the evenRule is empty the program will not run.
     * Uses ScreenPrinter to make a message when that happens.
     *
     * @param AppValues, ApplicationValues&. Ref to the collection of current values used program settings.
     * @param value, char*. Value used after the argument switch argument
     *
     * @test
     * Correctly saves the name for rule
     * If rule name is valid is get used for program (on even)
     * If empty evenRule program wont start
     * Test what happens if you try different input that the expected name of a rule
    */
    void execute(ApplicationValues& appValues, char* evenRule);
};

/**
 * @brief  Rule used for odd generations
 *
 * @details
 * Child from BaseArgument class.
 * Handles the argument -or if it is entered.
 * Used to set what rule should be used for odd generations
 *
 * @test
 * Check that this argument can be created.
 * That correct argument value is returned.
 */
class OddRuleArgument : public BaseArgument {
public:

    /**
     * @brief OddRule Argument creator
     *
     * @details
     * The BaseArgument string need to be "-or"
     * Creates an Oddrule Argument instance.
     *
     * @param BaseArgument("-or")
     *
    */
    OddRuleArgument() : BaseArgument("-or") {}

    /**
     * @brief OddRule Argument deconstruct
     *
     * @details
     * A normal deconstruct for the argument class.
     *
     * @param VOID
    */
    ~OddRuleArgument() {}

    /**
     * @brief Execute the odd rule argument
     *
     * @details
     * Sets the oddRule to the oddRuleName variable.
     * It do not check that is a valid rule name.
     * If the oddRule is empty the program will not run.
     * Uses ScreenPrinter to make a message when that happens.
     *
     * @param AppValues, ApplicationValues&. Ref to the collection of current values used program settings.
     * @param value, char*. Value used after the argument switch argument
     *
     * @test
     * Correctly saves the name for rule
     * If rule name is valid is get used for program (on odd)
     * If empty oddRule program wont start
     * Test what happens if you try different input that the expected name of a rule
    */
    void execute(ApplicationValues& appValues, char* oddRule);
};

#endif //GAMEOFLIFE_MAINARGUMENTS_H
