/**
 * @file   FileLoader.h
 * @Author Erik Ström
 * @date   October 2017
 * @version 0.1
*/
#ifndef FileLoaderH
#define FileLoaderH

#include <map>
#include "Cell_Culture/Cell.h"
#include "Globals.h"

using namespace std;

/**
 * @brief  Class that imports data from file
 *
 * @details
 * Determines starting values for simulation, based on contents of specified file.
 *
 * Reads startup values from specified file, containing values for WORLD_DIMENSIONS and cell Population.
 * Will create the corresponding cells.
 *
 * The first row of the inputfile should be the world-dimensions (excluding rimcells) in the format WxH.
 * The following rows shouls consists of 1's and 0's representing the grid of living and dead cells.
 *
 * @test
 * If file is not found or not good, error should be thrown and application stop.
 */
class FileLoader {

public:
    /**
     * @brief  FileLoader constructor
     *
     * @details
     *  Creates nothing. Only creates object for further use.
     *
     * @param VOID
    */
    FileLoader() {}

    /**
     * @brief  Get population from a file
     *
     * @details
     *  Uses the global variable for file name to get the file.
     *  Returns an entire map of cells based on the file.
     *
     * @param cells, map<Point, Cell>&. Reference to what map the file data should be loaded to.
     *
     * @throws ios_base::failure if the inputfile cannot be found.
     *
     * @throws runtime_error If the file is invalid.
     *
     *
     * @test
     * If file is not found or not good, error should be thrown and application stop.
     * Load a file and see it make the expected result.
    */
    void loadPopulationFromFile(map<Point, Cell>& cells);

};

#endif
