/**
 * @file   SupportStructures.h
 * @Author Erik Ström
 * @date   October 2017
 * @version 0.1
 *
 * @brief  File for containing support structures
 *
 * @details
 * Containing the construct for Point. Used for location tracking in the population.
 * Containing the construct for Dimensions. Used for setting population size.
*/
#ifndef GAMEOFLIFE_SUPPORTSTRUCTURES_H
#define GAMEOFLIFE_SUPPORTSTRUCTURES_H

struct Point {
    int x, y;

    // Overloading operator < for comparing Point objects
    bool operator < (const Point& other) const {
        if (x == other.x)
            return y < other.y;
        return x < other.x;
    }

};

/*
Data structure holding information about world dimensions in pixels.
*/
struct Dimensions {
    int WIDTH, HEIGHT;
};


#endif //GAMEOFLIFE_SUPPORTSTRUCTURES_H
