#include "catch.hpp"
#include "Cell_Culture/Population.h"
#include <vector>
#include <algorithm>
#include <fstream>


SCENARIO("Increasing generation") {
    GIVEN("Default created and initiated population") {
        Population testPop;
        testPop.initiatePopulation("conway");

        THEN("Population should not be 0"){
            REQUIRE(testPop.getTotalCellPopulation() != 0);
        }

        WHEN("Making new generation") {

            THEN("Generations should increase by 1.") {
                REQUIRE(testPop.calculateNewGeneration() == 1);
                REQUIRE(testPop.calculateNewGeneration() == 2);
            }

            THEN("Increasing generation should not throw error"){
                REQUIRE_NOTHROW(testPop.calculateNewGeneration());
            }
        }
    }
}


SCENARIO("Creating population on size") {
    GIVEN("Population created by default values") {
        Population testPopNormal;
        testPopNormal.initiatePopulation("conway");

        THEN("It should have the default set size.") {
            // default size is (world width + 2) * (world height + 2)
            REQUIRE(testPopNormal.getTotalCellPopulation() == (WORLD_DIMENSIONS.HEIGHT + 2)*(WORLD_DIMENSIONS.WIDTH + 2));
        }
    }

    GIVEN("Population created by file") {
        Population testPopFile;

        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if(!f.good()){
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        testPopFile.initiatePopulation("conway");

        THEN("It should have the default set size.") {
            // file dimensions is 8x8 = 10x10 = 100 cells
            REQUIRE(testPopFile.getTotalCellPopulation() == 100);
        }
        // unset the global file name
        fileName.clear();
    }
}

SCENARIO("Get cell from position") {
    GIVEN("Population based on file") {
    Population testPopFile;

    // this require external file for testing
    fileName = "test/test-population-initial-state.txt"; // file on Linux
    std::ifstream f(fileName);
    if(!f.good()) {
        fileName = "../test/test-population-initial-state.txt"; // file on windows
    }
    f.close();

    testPopFile.initiatePopulation("conway");

        THEN("Cell 1-1 should be dead") {
            REQUIRE_FALSE((testPopFile.getCellAtPosition(Point{1,1})).isAlive());
        }
        THEN("Cell 2-2 should be alive") {
            REQUIRE((testPopFile.getCellAtPosition(Point{2,2})).isAlive());
        }
        THEN("Cell 3-3 should be dead") {
            REQUIRE_FALSE((testPopFile.getCellAtPosition(Point{3,3})).isAlive());
        }
    }
    fileName.clear();
}

// Cant get rule setting out of population
// Not completing this test
SCENARIO("Adding rules to create populations") {
    GIVEN("Population created with wrong rule string"){
        Population testPop;
        testPop.initiatePopulation("thisIsNotAValidString");
        THEN("Should have default rule (conway) for both rules.") {
            // conway

        }
    }
    GIVEN("Population created with conway"){
        Population testPop;
        testPop.initiatePopulation("conway");
        THEN("Should use conway for both rules.") {
            // conway
        }
    }
    GIVEN("Population created with conway"){
        Population testPop;
        testPop.initiatePopulation("von_neumann");
        THEN("Should use von_neumann for both rules.") {
            // von_neumann
        }
    }
    GIVEN("Population created with erik"){
        Population testPop;
        testPop.initiatePopulation("erik");
        THEN("Should use erik for both rules.") {
            // erik
        }
    }

    GIVEN("Population created two rules conway"){
        Population testPop;
        testPop.initiatePopulation("erik", "von_neumann");
        THEN("First rule should be erik") {
            // erik
        }
        THEN("Second rule should be von_neumann") {
            // von_neumann
        }
    }

}

SCENARIO("Using an empty population") {
    GIVEN("An empty population") {
        Population emptyPop;

        THEN("The total cell count should be 0.") {
            REQUIRE(emptyPop.getTotalCellPopulation() == 0);
        }

        WHEN("Making new generation") {
            THEN("Then error should be thrown.") {
                REQUIRE_THROWS( emptyPop.calculateNewGeneration());
            }
        }
    }
}
