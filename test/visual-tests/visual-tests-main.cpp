#include <iostream>
#include "visual-tests.h"

using namespace std;

int main()
{
    VisualTests::GameOfLifeVisualTest();
    VisualTests::ScreenPrinterVisualTest();
    VisualTests::MainArgumentsParserVisualTest();

    return 0;

}
