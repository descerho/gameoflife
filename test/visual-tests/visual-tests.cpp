#include <iostream>
#include <fstream>
#include "visual-tests.h"
#include "../../include/GameOfLife.h"
#include "../../include/ScreenPrinter.h"
#include "../../include/Support/MainArgumentsParser.h"

using namespace std;

void VisualTests::GameOfLifeVisualTest()
{

    GameOfLife gof(50, "conway", "erik");
    gof.runSimulation();
    cout<<endl<<endl<<"GameOfLife(50, \"conway\", \"\")" << endl;
    cout<<"The screen should have been cleared and presented a grid of changing Cells" << endl;
    cout<<"Press enter to continue";
    cin.get();


    GameOfLife gof2(-50, "conway", "erik");
    gof2.runSimulation();
    cout<<endl<<endl<<"GameOfLife(-50, \"conway\", \"erik\")" << endl;
    cout<<"The screen should have been cleared" << endl;
    cout<<"Press enter to continue";
    cin.get();


    GameOfLife gof3(50, "klas-göran", "erik");
    gof3.runSimulation();
    cout<<endl<<endl<<"GameOfLife(50, \"klas-göran\", \"erik\")" << endl;
    cout<<"The screen should have been cleared and presented a grid of changing Cells" << endl;
    cout<<"Press enter to continue";
    cin.get();


    GameOfLife gof4(50, "von_neumann", "klas-göran");
    gof4.runSimulation();
    cout<<endl<<endl<<"GameOfLife(50, \"von_neumann\", \"klas-göran\")" << endl;
    cout<<"The screen should have been cleared and presented a grid of changing Cells" << endl;
    cout<<"Press enter to continue";
    cin.get();


    GameOfLife gof5(-50, "klas-göran", "göran");
    gof5.runSimulation();
    cout<<endl<<endl<<"GameOfLife(-50, \"klas-göran\", \"göran\")" << endl;
    cout<<"The screen should have been cleared" << endl;
    cout<<"Press enter to continue";
    cin.get();
}

void VisualTests::ScreenPrinterVisualTest()
{
    ScreenPrinter screenPrinter = ScreenPrinter::getInstance();
    screenPrinter.clearScreen();
    cout<<"ScreenPrinter::clearScreen(): Apart from this message, the screen should be completely cleared. Press enter to continue."<<endl;
    cin.get();

    screenPrinter.printHelpScreen();
    cout<<endl<<"ScreenPrinter::printHelpScreen(): A helpscreen containing the different arguments and options that are available should be listed above."<<endl;
    cout << "Press enter to continue"<<endl<<endl;
    cin.get();

    screenPrinter.printMessage("This is a message");
    cout<<endl<<"ScreenPrinter::printMessage(""This is a message""): The message ""This is a message"" should appear above."<<endl;
    cout<<"Press enter to continue"<<endl;
    cin.get();

    fileName="test/visual-tests/screenprinter-population-10x10.txt";
    std::ifstream f(fileName);
    if(!f.good()){

        fileName = "../test/visual-tests/screenprinter-population-10x10.txt"; // file on windows

    }

    Population population;
    population.initiatePopulation("conway");
    screenPrinter.clearScreen();
    screenPrinter.printBoard(population);
    cout << endl << "ScreenPrinter::printBoard(population): The screen should show a 10x10 population-board where every row and column contains exactly 5 markers(cells) separated by a blank space"<<endl;
    cout<<"Press enter to continue";
    cin.get();
    f.close();


    fileName="test/visual-tests/screenprinter-population-20x5.txt";
    f.open(fileName);
    if(!f.good()){

        fileName = "../test/visual-tests/screenprinter-population-20x5.txt"; // file on windows

    }
    population.initiatePopulation("conway");
    screenPrinter.clearScreen();
    screenPrinter.printBoard(population);
    cout << endl << "ScreenPrinter::printBoard(population): The screen should show a 20x5 population-board where markers(cells) form a rectangle with a single blank line in the middle. It should be bordered by blank cells."<<endl;
    cout<<"Press enter to continue";
    cin.get();
    f.close();


    fileName="test/visual-tests/screenprinter-population-5x20.txt";
    f.open(fileName);
    if(!f.good()){

        fileName = "../test/visual-tests/screenprinter-population-5x20.txt"; // file on windows

    }
    population.initiatePopulation("conway");
    screenPrinter.clearScreen();
    screenPrinter.printBoard(population);
    cout << endl << "ScreenPrinter::printBoard(population): The screen should show a 5x20 population-board where markers(cells) form a rectangle with a single blank line in the middle. It should be bordered by blank cells."<<endl;
    cout<<"Press enter to continue";
    cin.get();
    f.close();

    


}

void VisualTests::MainArgumentsParserVisualTest() {
    MainArgumentsParser parser;

    std::vector<std::string> arguments = {"-h"};

    std::vector<char*> argv;
    for (const auto& arg : arguments)
        argv.push_back((char*)arg.data());
    argv.push_back(nullptr);

    parser.runParser(argv.data(), argv.size()-1);

    cout<<endl<<endl<<"MainArgumentsParser, -h argument: The screen should show a help screen with the different arguments and options"<<endl;
    cout << "Press enter to continue";
    cin.get();

    arguments.clear();
    argv.clear();

    arguments = {"-g", "-f", "-s", "-er", "-or"};

    
    for (const auto& arg : arguments)
        argv.push_back((char*)arg.data());
    argv.push_back(nullptr);

    parser.runParser(argv.data(), argv.size()-1);

    cout << endl << endl <<"MainArgumentsParser, all arguments except -h, no values" << endl;
    cout << "There should be 5 messages on screen explaining that values are missing for the arguments." << endl;
    cout << "Press enter to continue";
    cin.get();
}
