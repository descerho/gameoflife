#ifndef __VISUAL_TESTS_H
#define __VISUAL_TESTS_H


class VisualTests
{
    public:
        static void GameOfLifeVisualTest();
        static void ScreenPrinterVisualTest();
        static void MainArgumentsParserVisualTest();
};


#endif
