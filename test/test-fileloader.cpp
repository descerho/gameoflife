#include "catch.hpp"
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <map>
#include "Support/FileLoader.h"
#include "Cell_Culture/Cell.h"

TEST_CASE("Loading valid files should give the correct result")
{
    map<Point, bool> aliveState = {
        {{0,0}, false},{{1,0}, false},{{2,0}, false},{{3,0}, false},{{4,0}, false},{{5,0}, false},{{6,0}, false},{{7,0}, false},{{8,0}, false},{{9,0}, false},
        {{0,1}, false},{{1,1}, false},{{2,1}, false},{{3,1}, false},{{4,1}, false},{{5,1}, false},{{6,1}, false},{{7,1}, false},{{8,1}, false},{{9,1}, false},
        {{0,2}, false},{{1,2}, false},{{2,2}, true },{{3,2}, false},{{4,2}, false},{{5,2}, true },{{6,2}, true },{{7,2}, false},{{8,2}, false},{{9,2}, false},
        {{0,3}, false},{{1,3}, false},{{2,3}, false},{{3,3}, false},{{4,3}, false},{{5,3}, false},{{6,3}, false},{{7,3}, true },{{8,3}, false},{{9,3}, false},
        {{0,4}, false},{{1,4}, false},{{2,4}, true },{{3,4}, false},{{4,4}, false},{{5,4}, true },{{6,4}, true },{{7,4}, false},{{8,4}, false},{{9,4}, false},
        {{0,5}, false},{{1,5}, false},{{2,5}, false},{{3,5}, true },{{4,5}, true },{{5,5}, true },{{6,5}, false},{{7,5}, true },{{8,5}, false},{{9,5}, false},
        {{0,6}, false},{{1,6}, false},{{2,6}, false},{{3,6}, false},{{4,6}, false},{{5,6}, true },{{6,6}, false},{{7,6}, true },{{8,6}, false},{{9,6}, false},
        {{0,7}, false},{{1,7}, false},{{2,7}, false},{{3,7}, false},{{4,7}, true },{{5,7}, true },{{6,7}, false},{{7,7}, false},{{8,7}, false},{{9,7}, false},
        {{0,8}, false},{{1,8}, false},{{2,8}, false},{{3,8}, false},{{4,8}, false},{{5,8}, false},{{6,8}, false},{{7,8}, false},{{8,8}, false},{{9,8}, false},
        {{0,9}, false},{{1,9}, false},{{2,9}, false},{{3,9}, false},{{4,9}, false},{{5,9}, false},{{6,9}, false},{{7,9}, false},{{8,9}, false},{{9,9}, false},
                                    };
    map<Point, Cell> cells;
    FileLoader fileLoader;

    SECTION("Loading from the valid file ""test/test-population-initial-state.txt""") {

        fileName = "test/test-population-initial-state.txt";
        fstream f(fileName);
        if (!f.good()) // In case the tests are built in a subdirectory
            fileName = "../test/test-population-initial-state.txt";
        f.close();

        fileLoader.loadPopulationFromFile(cells);

        std::ostringstream oss;
        for (int col=0; col <= WORLD_DIMENSIONS.WIDTH + 1; col++) {
            for(int row=0; row <= WORLD_DIMENSIONS.HEIGHT + 1; row++) {
                oss << "The point at (" << col << "," << row << ") should have correct alive-state"; 

                SECTION(oss.str()) {
                    REQUIRE(cells.at(Point{col, row}).isAlive() == aliveState.at(Point{col, row}));
                }
                oss.str("");

            }

        }

        for (int col=0; col <= WORLD_DIMENSIONS.WIDTH + 1; col++) {
            for(int row=0; row <= WORLD_DIMENSIONS.HEIGHT + 1; row++) {
                oss << "The point at (" << col << "," << row << ") should have correct rimCell-state"; 

                SECTION(oss.str()) {
                    if (row == 0 || col == 0 || row == WORLD_DIMENSIONS.HEIGHT + 1 || col == WORLD_DIMENSIONS.WIDTH + 1)
                        REQUIRE(cells.at(Point{col, row}).isRimCell() == true);
                    else
                        REQUIRE(cells.at(Point{col, row}).isRimCell() == false);
                }
                oss.str("");

            }

        }
    }

    SECTION("Loading from the valid file ""test/fileloader-test-added-cells.txt""") { 
        /* This is the same file as in the above section with an added cell in the last row of the next to last column.
         * There is also an added row.
         * This class should ignore the added cell and read the subset of the file according
         * to the world dimensions that are determined in the beginning of the file.
         * Therefore the result should be the same as in the section above.*/

        fileName = "test/fileloader-test-added-cells.txt";
        fstream f(fileName);
        if (!f.good()) // In case the tests are built in a subdirectory
            fileName = "../test/fileloader-test-added-cells.txt";
        f.close();

        fileLoader.loadPopulationFromFile(cells);

        std::ostringstream oss;
        for (int col=0; col <= WORLD_DIMENSIONS.WIDTH + 1; col++) {
            for(int row=0; row <= WORLD_DIMENSIONS.HEIGHT + 1; row++) {
                oss << "The point at (" << col << "," << row << ") should have correct alive-state"; 

                SECTION(oss.str()) {
                    REQUIRE(cells.at(Point{col, row}).isAlive() == aliveState.at(Point{col, row}));
                }
                oss.str("");

            }

        }

        for (int col=0; col <= WORLD_DIMENSIONS.WIDTH + 1; col++) {
            for(int row=0; row <= WORLD_DIMENSIONS.HEIGHT + 1; row++) {
                oss << "The point at (" << col << "," << row << ") should have correct rimCell-state"; 

                SECTION(oss.str()) {
                    if (row == 0 || col == 0 || row == WORLD_DIMENSIONS.HEIGHT + 1 || col == WORLD_DIMENSIONS.WIDTH + 1)
                        REQUIRE(cells.at(Point{col, row}).isRimCell() == true);
                    else
                        REQUIRE(cells.at(Point{col, row}).isRimCell() == false);
                }
                oss.str("");

            }

        }
    }
}

TEST_CASE("Load from invalid files should throw an exception")
{
    FileLoader fileLoader;
    map<Point, Cell> cells;

    SECTION("The global fileName not set") {
        fileName.clear();
        REQUIRE_THROWS(fileLoader.loadPopulationFromFile(cells));
    }

    SECTION("Non-existent file") {
        fileName = "this.is.not.a.file";
        REQUIRE_THROWS(fileLoader.loadPopulationFromFile(cells));
    }
    
    SECTION("The file misses world dimensions (test/fileloader-test-missing-dimensions.txt)") {

        fileName = "test/fileloader-test-missing-dimensions.txt";
        fstream f(fileName);
        if (!f.good()) // In case the tests are built in a subdirectory
            fileName = "../test/fileloader-test-missing-dimensions.txt";
        f.close();
        REQUIRE_THROWS(fileLoader.loadPopulationFromFile(cells));
    }

    SECTION("The cell-structure in the file misses a row accrording to its world dimensions (test/fileloader-test-missing-row.txt)") {

        fileName = "test/fileloader-test-missing-row.txt";
        fstream f(fileName);
        if (!f.good()) // In case the tests are built in a subdirectory
            fileName = "../test/fileloader-test-missing-row.txt";
        f.close();
        REQUIRE_THROWS(fileLoader.loadPopulationFromFile(cells));
    }

    SECTION("The cell-structure in the file is missing one cell (test/fileloader-test-missing-cell.txt)") {

        fileName = "test/fileloader-test-missing-cell.txt";
        fstream f(fileName);
        if (!f.good()) // In case the tests are built in a subdirectory
            fileName = "../test/fileloader-test-missing-cell.txt";
        f.close();
        REQUIRE_THROWS(fileLoader.loadPopulationFromFile(cells));
    }

fileName.clear();
}



