#include "catch.hpp"
#include <vector>
#include <algorithm>
#include "ScreenPrinter.h"

TEST_CASE("Only a single object of ScreenPrinter is allowed and that reference should be returned by getInstance")
{
    ScreenPrinter& screenPrinter1 = ScreenPrinter::getInstance();
    ScreenPrinter& screenPrinter2 = ScreenPrinter::getInstance();

    REQUIRE(&screenPrinter1 == &screenPrinter2);
}
