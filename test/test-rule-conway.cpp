#include "catch.hpp"
#include <vector>
#include <algorithm>
#include <Cell_Culture/Population.h>
#include <fstream>
#include <GoL_Rules/RuleOfExistence_Conway.h>
#include <Support/FileLoader.h>
#include <map>

SCENARIO("Creating conway rules") {
    GIVEN("Rules giving to existing cells") {
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        FileLoader fileLoader;
        fileLoader.loadPopulationFromFile(cellMap);

        RuleOfExistence_Conway ruleOfExistenceConway(cellMap);
        THEN("Rule name should be Conway.") {
            REQUIRE(ruleOfExistenceConway.getRuleName() == "conway");
        }
    }

    GIVEN("Rules given to empty cells") {
        map<Point, Cell> cellMapEmpty;

        RuleOfExistence_Conway ruleOfExistenceConway(cellMapEmpty);
        THEN("Rule name should be Conway.") {
            REQUIRE(ruleOfExistenceConway.getRuleName() == "conway");
        }
    }

    fileName.clear();
}


SCENARIO("Aging cells have the correct rules applied to them") {
    GIVEN("Two set of cells, one before aging and one of the expected results") {
        FileLoader fileLoader;
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        fileLoader.loadPopulationFromFile(cellMap);

        fileName = "test/test-population-conway.txt"; // file on Linux
        std::ifstream secondFile(fileName);
        if (!secondFile.good()) {
            fileName = "../test/test-population-conway.txt"; // file on windows
        }
        secondFile.close();

        map<Point, Cell> cellMapResult;
        fileLoader.loadPopulationFromFile(cellMapResult);

        WHEN("Comparing the cells."){
            THEN("Should have the same amount of cells."){
                REQUIRE(cellMap.size() == cellMapResult.size());

                AND_WHEN("Aging the original cells."){
                    RuleOfExistence_Conway ruleOfExistenceConway(cellMap);
                    ruleOfExistenceConway.executeRule();
                    for (auto& current : cellMap) {
                        current.second.updateState();
                    }

                    THEN("Cells should have the same value as the imported result file."){
                        bool compareBool = true;
                        typename map<Point, Cell>::iterator i, j;
                        for(i = cellMap.begin(), j = cellMapResult.begin(); i != cellMap.end(); ++i, ++j)
                        {
                            if((i)->second.isAlive() != (j)->second.isAlive()){
                                compareBool = false;
                            }
                        };

                        REQUIRE(compareBool);
                    }
                }
            }
        }
    }

    GIVEN("Two set of cells, one before aging and one of the wrong expected results") {
        FileLoader fileLoader;
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        fileLoader.loadPopulationFromFile(cellMap);

        fileName = "test/test-population-von_neumann.txt"; // file on Linux
        std::ifstream secondFile(fileName);
        if (!secondFile.good()) {
            fileName = "../test/test-population-von_neumann.txt"; // file on windows
        }
        secondFile.close();

        map<Point, Cell> cellMapResult;
        fileLoader.loadPopulationFromFile(cellMapResult);

        WHEN("Comparing the cells."){
            THEN("Should have the same amount of cells."){
                REQUIRE(cellMap.size() == cellMapResult.size());

                AND_WHEN("Aging the original cells."){
                    RuleOfExistence_Conway ruleOfExistenceConway(cellMap);
                    ruleOfExistenceConway.executeRule();
                    for (auto& current : cellMap) {
                        current.second.updateState();
                    }

                    THEN("Cells should not be the same as the imported result."){
                        bool compareBool = true;
                        typename map<Point, Cell>::iterator i, j;
                        for(i = cellMap.begin(), j = cellMapResult.begin(); i != cellMap.end(); ++i, ++j)
                        {
                            if((i)->second.isAlive() != (j)->second.isAlive()){
                                compareBool = false;
                            }
                        };

                        REQUIRE_FALSE(compareBool);
                    }
                }
            }
        }
    }

    fileName.clear();
}

