#include "catch.hpp"
#include <vector>
#include <algorithm>
#include <Cell_Culture/Population.h>
#include <fstream>
#include <GoL_Rules/RuleOfExistence_Erik.h>
#include <Support/FileLoader.h>
#include <map>
#include <Cell_Culture/Cell.h>

SCENARIO("Creating cells based on Eriks rules") {
    GIVEN("Rules giving to existing cells") {
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        FileLoader fileLoader;
        fileLoader.loadPopulationFromFile(cellMap);

        RuleOfExistence_Erik ruleOfExistenceErik(cellMap);
        THEN("Rule name should be Erik.") {
            REQUIRE(ruleOfExistenceErik.getRuleName() == "erik");
        }
    }

    GIVEN("Rules given to empty cells") {
        map<Point, Cell> cellMapEmpty;

        RuleOfExistence_Erik ruleOfExistenceErik(cellMapEmpty);
        THEN("Rule name should be Erik.") {
            REQUIRE(ruleOfExistenceErik.getRuleName() == "erik");
        }
    }

    fileName.clear();
}


SCENARIO("Aging cells based on Erik's rule have the correct rules applied to them") {
    GIVEN("Two set of cells, one before aging and one of the expected results") {
        FileLoader fileLoader;
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        fileLoader.loadPopulationFromFile(cellMap);

        fileName = "test/test-population-erik.txt"; // file on Linux
        std::ifstream secondFile(fileName);
        if (!secondFile.good()) {
            fileName = "../test/test-population-erik.txt"; // file on windows
        }
        secondFile.close();

        map<Point, Cell> cellMapResult;
        fileLoader.loadPopulationFromFile(cellMapResult);

        WHEN("Comparing the cells."){
            THEN("Should have the same amount of cells."){
                REQUIRE(cellMap.size() == cellMapResult.size());

                AND_WHEN("Aging the original cells."){
                    RuleOfExistence_Erik ruleOfExistenceErik(cellMap);
                    ruleOfExistenceErik.executeRule();
                    for (auto& current : cellMap) {
                        current.second.updateState();
                    }

                    THEN("Cells should have the same value as the imported result file."){
                        bool compareBool = true;
                        typename map<Point, Cell>::iterator i, j;
                        for(i = cellMap.begin(), j = cellMapResult.begin(); i != cellMap.end(); ++i, ++j)
                        {
                            if((i)->second.isAlive() != (j)->second.isAlive()){
                                compareBool = false;
                            }
                        };

                        REQUIRE(compareBool);
                    }
                }
            }
        }
    }

    GIVEN("Two set of cells, one before aging and one of the wrong expected results") {
        FileLoader fileLoader;
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        fileLoader.loadPopulationFromFile(cellMap);

        fileName = "test/test-population-von_neumann.txt"; // file on Linux
        std::ifstream secondFile(fileName);
        if (!secondFile.good()) {
            fileName = "../test/test-population-von_neumann.txt"; // file on windows
        }
        secondFile.close();

        map<Point, Cell> cellMapResult;
        fileLoader.loadPopulationFromFile(cellMapResult);

        WHEN("Comparing the cells."){
            THEN("Should have the same amount of cells."){
                REQUIRE(cellMap.size() == cellMapResult.size());

                AND_WHEN("Aging the original cells."){
                    RuleOfExistence_Erik ruleOfExistenceErik(cellMap);
                    ruleOfExistenceErik.executeRule();
                    for (auto& current : cellMap) {
                        current.second.updateState();
                    }

                    THEN("Cells should not be the same as the imported result."){
                        bool compareBool = true;
                        typename map<Point, Cell>::iterator i, j;
                        for(i = cellMap.begin(), j = cellMapResult.begin(); i != cellMap.end(); ++i, ++j)
                        {
                            if((i)->second.isAlive() != (j)->second.isAlive()){
                                compareBool = false;
                            }
                        };

                        REQUIRE_FALSE(compareBool);
                    }
                }
            }
        }
    }

    fileName.clear();
}

SCENARIO("Cells getting older change color and value") {
    GIVEN("Cell map based on eric rules") {
        map<Point, Cell> cellMap;

        cellMap[Point{1, 1}] = Cell(true);
        cellMap[Point{2, 1}] = Cell(true);
        cellMap[Point{3, 1}] = Cell(true);
        cellMap[Point{4, 1}] = Cell(true);
        cellMap[Point{5, 1}] = Cell(true);
        cellMap[Point{6, 1}] = Cell(true);
        cellMap[Point{1, 2}] = Cell(true);
        cellMap[Point{2, 2}] = Cell(false, GIVE_CELL_LIFE);
        cellMap[Point{3, 2}] = Cell(false, GIVE_CELL_LIFE);
        cellMap[Point{4, 2}] = Cell(false, GIVE_CELL_LIFE);
        cellMap[Point{5, 2}] = Cell(false, GIVE_CELL_LIFE);
        cellMap[Point{6, 2}] = Cell(true);
        cellMap[Point{1, 3}] = Cell(true);
        cellMap[Point{2, 3}] = Cell(true);
        cellMap[Point{3, 3}] = Cell(true);
        cellMap[Point{4, 3}] = Cell(true);
        cellMap[Point{5, 3}] = Cell(true);
        cellMap[Point{6, 3}] = Cell(true);
        /*
         * OOOOO0
         * 0####0
         * 000000
         */

        // add rules to them
        RuleOfExistence_Erik ruleOfExistenceErik(cellMap);

        WHEN("Aging cells to set age 4") {
            for (int i = 0; i < 4; i++) {
                ruleOfExistenceErik.executeRule();
                cellMap.at({4, 2}).updateState();
                cellMap.at({3, 2}).updateState();
            }
            THEN("Changed cell should not be cyan.") {
                REQUIRE_FALSE((cellMap.at({4, 2})).getColor() == COLOR::CYAN);
                REQUIRE_FALSE((cellMap.at({3, 2})).getColor() == COLOR::CYAN);
            }

            AND_WHEN("Aging cells to age 5") {
                ruleOfExistenceErik.executeRule();
                cellMap.at({4, 2}).updateState();
                cellMap.at({3, 2}).updateState();

                THEN("Changed cell should be cyan.") {
                    REQUIRE((cellMap.at({4, 2})).getColor() == COLOR::CYAN);
                    REQUIRE((cellMap.at({3, 2})).getColor() == COLOR::CYAN);
                }

                AND_WHEN("Aging cells to age 9") {
                    for (int i = 0; i < 4; i++) {
                        ruleOfExistenceErik.executeRule();
                        cellMap.at({4, 2}).updateState();
                        cellMap.at({3, 2}).updateState();
                    }
                    THEN("Changed cells should still have default value.") {
                        REQUIRE((cellMap.at({4, 2})).getCellValue() == '#');
                        REQUIRE((cellMap.at({3, 2})).getCellValue() == '#');
                    }

                    AND_WHEN("Aging cell to 10") {
                        ruleOfExistenceErik.executeRule();
                        cellMap.at({4, 2}).updateState();

                        THEN("Changed cell should now have value E.") {
                            REQUIRE((cellMap.at({4, 2})).getCellValue() == 'E');
                        }

                        AND_WHEN("Aging cell to 10 and 12") {
                            for (int i = 0; i < 3; i++) {
                                ruleOfExistenceErik.executeRule();
                                cellMap.at({3, 2}).updateState();
                            }
                            THEN("The old cell should be prime elder.") {
                                REQUIRE((cellMap.at({3, 2})).getColor() == COLOR::MAGENTA);
                            }

                            AND_WHEN("Killing the cell") {
                                // Manually killing the cell next to it
                                cellMap.at({2, 2}).setNextGenerationAction(KILL_CELL);
                                cellMap.at({2, 2}).updateState();
                                // The Erik rule will now kill the cell used for checking, it only has 1 friend left
                                ruleOfExistenceErik.executeRule();
                                cellMap.at({3, 2}).updateState();

                                THEN("Color and value should reset.") {
                                    REQUIRE((cellMap.at({3, 2})).getColor() == COLOR::BLACK);
                                    REQUIRE((cellMap.at({3, 2})).getCellValue() == '#');
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

