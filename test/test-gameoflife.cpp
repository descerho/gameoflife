#include "catch.hpp"
#include "GameOfLife.h"
#include <vector>
#include <algorithm>

TEST_CASE("Given normal input the constructor should not break")
{
    GameOfLife gameOfLife(50, "conway", "erik");
}

TEST_CASE("Given abnormal input the constructor should not break")
{
    SECTION("Negative number of generations") 
    {
        GameOfLife gameOfLife(-50, "conway", "erik");
    }

    SECTION("Invalid evenRuleName")
    {
        GameOfLife gameOfLife(50, "klas-göran", "erik");
    }

    SECTION("Invalid oddRuleName")
    {
        GameOfLife gameOfLife(50, "von_neumann", "klas-göran");
    }

    SECTION("All invalid parameters at once") 
    {
        GameOfLife gameOfLife(-50, "klas-göran", "göran");
    }
}
