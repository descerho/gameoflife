#include "catch.hpp"
#include <vector>
#include <algorithm>
#include <Cell_Culture/Population.h>
#include <fstream>
#include <GoL_Rules/RuleOfExistence_VonNeumann.h>
#include <Support/FileLoader.h>
#include <map>

SCENARIO("Creating cells with von_neumann rules") {
    GIVEN("Rules giving to existing cells") {
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        FileLoader fileLoader;
        fileLoader.loadPopulationFromFile(cellMap);

        RuleOfExistence_VonNeumann ruleOfExistenceVonNeumann(cellMap);
        THEN("Rule name should be Von Neumann.") {
            REQUIRE(ruleOfExistenceVonNeumann.getRuleName() == "von_neumann");
        }
    }

    GIVEN("Rules given to empty cells") {
        map<Point, Cell> cellMapEmpty;

        RuleOfExistence_VonNeumann ruleOfExistenceVonNeumann(cellMapEmpty);
        THEN("Rule name should be Von Neumann.") {
            REQUIRE(ruleOfExistenceVonNeumann.getRuleName() == "von_neumann");
        }
    }

    fileName.clear();
}


SCENARIO("Aging cells fo von_neumann have the correct rules applied to them") {
    GIVEN("Two set of cells, one before aging and one of the expected results") {
        FileLoader fileLoader;
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        fileLoader.loadPopulationFromFile(cellMap);

        fileName = "test/test-population-von_neumann.txt"; // file on Linux
        std::ifstream secondFile(fileName);
        if (!secondFile.good()) {
            fileName = "../test/test-population-von_neumann.txt"; // file on windows
        }
        secondFile.close();

        map<Point, Cell> cellMapResult;
        fileLoader.loadPopulationFromFile(cellMapResult);

        WHEN("Comparing the cells."){
            THEN("Should have the same amount of cells."){
                REQUIRE(cellMap.size() == cellMapResult.size());

                AND_WHEN("Aging the original cells."){
                    RuleOfExistence_VonNeumann ruleOfExistenceVonNeumann(cellMap);
                    ruleOfExistenceVonNeumann.executeRule();
                    for (auto& current : cellMap) {
                        current.second.updateState();
                    }

                    THEN("Cells should have the same value as the imported result file."){
                        bool compareBool = true;
                        typename map<Point, Cell>::iterator i, j;
                        for(i = cellMap.begin(), j = cellMapResult.begin(); i != cellMap.end(); ++i, ++j)
                        {
                            if((i)->second.isAlive() != (j)->second.isAlive()){
                                compareBool = false;
                            }
                        };

                        REQUIRE(compareBool);
                    }
                }
            }
        }
    }

    GIVEN("Two set of cells, one before aging of von_neumann and one of the wrong expected results") {
        FileLoader fileLoader;
        // this require external file for testing
        fileName = "test/test-population-initial-state.txt"; // file on Linux
        std::ifstream f(fileName);
        if (!f.good()) {
            fileName = "../test/test-population-initial-state.txt"; // file on windows
        }
        f.close();

        map<Point, Cell> cellMap;
        fileLoader.loadPopulationFromFile(cellMap);

        fileName = "test/test-population-conway.txt"; // file on Linux
        std::ifstream secondFile(fileName);
        if (!secondFile.good()) {
            fileName = "../test/test-population-conway.txt"; // file on windows
        }
        secondFile.close();

        map<Point, Cell> cellMapResult;
        fileLoader.loadPopulationFromFile(cellMapResult);

        WHEN("Comparing the cells."){
            THEN("Should have the same amount of cells."){
                REQUIRE(cellMap.size() == cellMapResult.size());

                AND_WHEN("Aging the original cells."){
                    RuleOfExistence_VonNeumann ruleOfExistenceVonNeumann(cellMap);
                    ruleOfExistenceVonNeumann.executeRule();
                    for (auto& current : cellMap) {
                        current.second.updateState();
                    }

                    THEN("Cells should not be the same as the imported result."){
                        bool compareBool = true;
                        typename map<Point, Cell>::iterator i, j;
                        for(i = cellMap.begin(), j = cellMapResult.begin(); i != cellMap.end(); ++i, ++j)
                        {
                            if((i)->second.isAlive() != (j)->second.isAlive()){
                                compareBool = false;
                            }
                        };

                        REQUIRE_FALSE(compareBool);
                    }
                }
            }
        }
    }

    fileName.clear();
}

