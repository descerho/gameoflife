#include "catch.hpp"
#include "Cell_Culture/Cell.h"
#include <vector>
#include <algorithm>
// File for testing cell class


SCENARIO("Creating cells and turning them alive") {
  GIVEN("Rim cell and center cell") {
    Cell rimCell(true);
    Cell notRimCell(false);

    THEN("New cells should count as dead") {
      REQUIRE_FALSE(rimCell.isAlive());
      REQUIRE_FALSE(notRimCell.isAlive());
    }
    THEN("Cells should be of age 0") {
      REQUIRE(rimCell.getAge() == 0);
      REQUIRE(notRimCell.getAge() == 0);
    }

    WHEN("Cells should be turned alive") {
      rimCell.setNextGenerationAction(GIVE_CELL_LIFE);
      notRimCell.setNextGenerationAction(GIVE_CELL_LIFE);

      rimCell.updateState();
      notRimCell.updateState();

      THEN("Not rim cell should be alive") {
        REQUIRE(notRimCell.isAlive());
      }

      THEN("Rim cell should still be dead") {
        REQUIRE_FALSE(rimCell.isAlive());
      }

      THEN("Live cell should have age 1, dead still 0") {
        REQUIRE(rimCell.getAge() == 0);
        REQUIRE(notRimCell.getAge() == 1);
      }
    }
  }
}

SCENARIO("Changing cells with next action on next generation.") {
  // possible status is KILL_CELL, IGNORE_CELL, GIVE_CELL_LIFE, DO_NOTHING
  // Note: Rim cells should never be able to be alive
  GIVEN("Dead and alive, center and rim cells with different actions.") {
    Cell deadRimCell_1(true);
    Cell deadRimCell_2(true);
    Cell deadRimCell_3(true);
    Cell deadRimCell_4(true);
    Cell aliveRimCell_1(true,GIVE_CELL_LIFE);
    Cell aliveRimCell_2(true,GIVE_CELL_LIFE);
    Cell aliveRimCell_3(true,GIVE_CELL_LIFE);
    Cell aliveRimCell_4(true,GIVE_CELL_LIFE);
    Cell deadNotRimCell_1(false);
    Cell deadNotRimCell_2(false);
    Cell deadNotRimCell_3(false);
    Cell deadNotRimCell_4(false);
    Cell aliveNotRimCell_1(false,GIVE_CELL_LIFE);
    Cell aliveNotRimCell_2(false,GIVE_CELL_LIFE);
    Cell aliveNotRimCell_3(false,GIVE_CELL_LIFE);
    Cell aliveNotRimCell_4(false,GIVE_CELL_LIFE);

    deadRimCell_1.setNextGenerationAction(KILL_CELL);
    deadRimCell_2.setNextGenerationAction(IGNORE_CELL);
    deadRimCell_3.setNextGenerationAction(GIVE_CELL_LIFE);
    deadRimCell_4.setNextGenerationAction(DO_NOTHING);
    aliveRimCell_1.setNextGenerationAction(KILL_CELL);
    aliveRimCell_2.setNextGenerationAction(IGNORE_CELL);
    aliveRimCell_3.setNextGenerationAction(GIVE_CELL_LIFE);
    aliveRimCell_4.setNextGenerationAction(DO_NOTHING);
    deadNotRimCell_1.setNextGenerationAction(KILL_CELL);
    deadNotRimCell_2.setNextGenerationAction(IGNORE_CELL);
    deadNotRimCell_3.setNextGenerationAction(GIVE_CELL_LIFE);
    deadNotRimCell_4.setNextGenerationAction(DO_NOTHING);
    aliveNotRimCell_1.setNextGenerationAction(KILL_CELL);
    aliveNotRimCell_2.setNextGenerationAction(IGNORE_CELL);
    aliveNotRimCell_3.setNextGenerationAction(GIVE_CELL_LIFE);
    aliveNotRimCell_4.setNextGenerationAction(DO_NOTHING);

    WHEN("Cells are set to KILL_CELL.") {
      deadRimCell_1.updateState();
      aliveRimCell_1.updateState();
      deadNotRimCell_1.updateState();
      aliveNotRimCell_1.updateState();

      THEN("All cells should be dead.") {
        REQUIRE_FALSE(deadRimCell_1.isAlive());
        REQUIRE_FALSE(aliveRimCell_1.isAlive());
        REQUIRE_FALSE(deadNotRimCell_1.isAlive());
        REQUIRE_FALSE(aliveNotRimCell_1.isAlive());
      }
    }

    WHEN("Cells are set to IGNORE_CELL.") {
      deadRimCell_2.updateState();
      aliveRimCell_2.updateState();
      deadNotRimCell_2.updateState();
      aliveNotRimCell_2.updateState();

      THEN("Dead should stay dead.") {
        REQUIRE_FALSE(deadRimCell_2.isAlive());
        REQUIRE_FALSE(aliveRimCell_2.isAlive());
        REQUIRE_FALSE(deadNotRimCell_2.isAlive());
      }
      THEN("Alive should stay alive.") {
        REQUIRE(aliveNotRimCell_2.isAlive());
      }
    }

    WHEN("Cells are set to GIVE_CELL_LIFE.") {
      deadRimCell_3.updateState();
      aliveRimCell_3.updateState();
      deadNotRimCell_3.updateState();
      aliveNotRimCell_3.updateState();

      THEN("Rim cells should stay dead.") {
        REQUIRE_FALSE(deadRimCell_3.isAlive());
        REQUIRE_FALSE(aliveRimCell_3.isAlive());
      }
      THEN("Center cells should be alive.") {
        REQUIRE(aliveNotRimCell_3.isAlive());
        REQUIRE(deadNotRimCell_3.isAlive());
      }
    }

    WHEN("Cells are set to DO_NOTHING") {
      deadRimCell_4.updateState();
      aliveRimCell_4.updateState();
      deadNotRimCell_4.updateState();
      aliveNotRimCell_4.updateState();

      THEN("Dead should stay dead.") {
        REQUIRE_FALSE(deadRimCell_4.isAlive());
        REQUIRE_FALSE(aliveRimCell_4.isAlive());
        REQUIRE_FALSE(deadNotRimCell_4.isAlive());
      }
      THEN("Alive should stay alive.") {
        REQUIRE(aliveNotRimCell_4.isAlive());
      }
    }
  }
}

SCENARIO("Cells created with set actions") {
  GIVEN("Cells created with Actions") {
    Cell cell_1(false);
    Cell cell_2(false, KILL_CELL);
    Cell cell_3(false, IGNORE_CELL);
    Cell cell_4(false, GIVE_CELL_LIFE);
    Cell cell_5(false, DO_NOTHING);

    THEN("Cell without argument be dead") {
      REQUIRE_FALSE(cell_1.isAlive());
    }
    THEN("Cells without Kill, IGNORE and NOTHING should be dead") {
      REQUIRE_FALSE(cell_2.isAlive());
      REQUIRE_FALSE(cell_3.isAlive());
      REQUIRE_FALSE(cell_5.isAlive());
    }
    THEN("Cells given life should be alive after constructor") {
      REQUIRE(cell_4.isAlive());
    }
  }
}

SCENARIO("Cell being alive next bool changes") {
  GIVEN("A new cell") {
    Cell aCell(false);

    THEN("Cells default should not be alive") {
      REQUIRE_FALSE(aCell.isAliveNext());
    }

    WHEN("Changed to alive next") {
      aCell.setIsAliveNext(true);

      THEN("It should return true.") {
        REQUIRE(aCell.isAliveNext());
      }
      AND_WHEN("Changed back to dead next") {
        aCell.setIsAliveNext(false);

        THEN("It should return false") {
          REQUIRE_FALSE(aCell.isAliveNext());
        }
      }
    }
  }
}

SCENARIO("Cell color changes") {
  GIVEN("A new dead cell and alive cell") {
    Cell deadCell(false);
    Cell aliveCell(false, GIVE_CELL_LIFE);

    THEN("They should have default color.") {
      REQUIRE(deadCell.getColor() == STATE_COLORS.DEAD);
      REQUIRE(aliveCell.getColor() == STATE_COLORS.LIVING);
    }
    WHEN("Changing color manually") {
      deadCell.setNextColor(STATE_COLORS.LIVING);
      deadCell.updateState();

      THEN("It should reflecet the change.") {
        REQUIRE(deadCell.getColor() == STATE_COLORS.LIVING);
      }
    }
  }
}

SCENARIO("Cell character changes") {
  GIVEN("A new cell") {
    Cell aCell(false);

    THEN("It should have default character.") {
      REQUIRE(aCell.getCellValue() == '#');
    }
    WHEN("Changing character manually") {
      aCell.setNextCellValue('$');
      aCell.updateState();

      THEN("It should reflecet the change.") {
        REQUIRE(aCell.getCellValue() == '$');
      }
      THEN("Not have any other char.") {
        REQUIRE(aCell.getCellValue() != '!');
      }
    }
  }
}

SCENARIO("Changing next generation action") {
  GIVEN("A new cell") {
    Cell aCell(false);

    THEN("Default all new cells should have DO_NOTHING as next action.") {
      REQUIRE(aCell.getNextGenerationAction() == DO_NOTHING);
    }
    WHEN("Changing it to KILL_CELL") {
      aCell.setNextGenerationAction(KILL_CELL);
        
      THEN("It should reflecet the change.") { 
        REQUIRE(aCell.getNextGenerationAction() == KILL_CELL);
      }
    }
      WHEN("Changing it to IGNORE_CELL") {
        aCell.setNextGenerationAction(IGNORE_CELL);
        THEN("It should reflecet the change.") {
          REQUIRE(aCell.getNextGenerationAction() == IGNORE_CELL);
        }
      }
      WHEN("Changing it to GIVE_CELL_LIFE") {
        aCell.setNextGenerationAction(GIVE_CELL_LIFE);

        THEN("It should reflecet the change.") {
          REQUIRE(aCell.getNextGenerationAction() == GIVE_CELL_LIFE);
        }
      }
      WHEN("Changing it to GIVE_CELL_LIFE and back to do nothing") {
        aCell.setNextGenerationAction(GIVE_CELL_LIFE);
        aCell.setNextGenerationAction(DO_NOTHING);
        THEN("It should reflecet the change to nothing.") {
          REQUIRE(aCell.getNextGenerationAction() == DO_NOTHING);
        }
      }
  }
}