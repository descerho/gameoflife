#include "catch.hpp"
#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
#include <fstream>
#include "Support/MainArgumentsParser.h"

SCENARIO("Valid arguments") {

    //Set defaults in case some other tests have messed with them
    fileName.clear();
    WORLD_DIMENSIONS.WIDTH=80;
    WORLD_DIMENSIONS.HEIGHT=24;

    MainArgumentsParser parser;
    ApplicationValues values;

    GIVEN("Nothing") {
        std::vector<std::string> arguments = {};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("The defaults should be returned") {
            REQUIRE(values.runSimulation == true);
            REQUIRE(values.evenRuleName == "conway");
            REQUIRE(values.oddRuleName  == "conway");
            REQUIRE(values.maxGenerations == 100);
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
        }
    }

    GIVEN("The argument -h") {
        std::vector<std::string> arguments = {"-h"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);
        THEN("runSimulation should be set to false") {

            REQUIRE(values.runSimulation == false);

            //Defaults
            REQUIRE(values.evenRuleName == "conway");
            REQUIRE(values.oddRuleName  == "conway");
            REQUIRE(values.maxGenerations == 100);
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

        }
    }

    GIVEN("The argument ""-g 10""") {
        std::vector<std::string> arguments = {"-g", "10"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("maxGenerations should be set to 10") {
            REQUIRE(values.maxGenerations == 10);

            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(values.evenRuleName == "conway");
            REQUIRE(values.oddRuleName  == "conway");
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

        }
    }

    GIVEN("The argument ""-s 10x10""") {
        std::vector<std::string> arguments = {"-s", "10x10"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("The world dimensions should be 10x10") {

            REQUIRE(WORLD_DIMENSIONS.WIDTH == 10);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 10);

            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(values.evenRuleName == "conway");
            REQUIRE(values.oddRuleName  == "conway");
            REQUIRE(values.maxGenerations == 100);
            REQUIRE(fileName == "");

        }
        WORLD_DIMENSIONS.WIDTH = 80;
        WORLD_DIMENSIONS.HEIGHT = 24;
    }

    GIVEN("The argument ""-f Population_Seed.txt""") {
        std::vector<std::string> arguments = {"-f", "Population_Seed.txt"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("The filename should be set to Population_Seed.txt") {

            REQUIRE(fileName == "Population_Seed.txt");

            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(values.evenRuleName == "conway");
            REQUIRE(values.oddRuleName  == "conway");
            REQUIRE(values.maxGenerations == 100);
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
        }
        fileName.clear();
    }

    GIVEN("The argument ""-er erik""") {
        std::vector<std::string> arguments = {"-er", "erik"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("The evenRuleName and oddRuleName should be set to erik") {

            REQUIRE(values.evenRuleName == "erik");
            REQUIRE(values.oddRuleName  == "erik");

            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(values.maxGenerations == 100);
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

        }
    }

    GIVEN("The argument ""-or erik""") {
        std::vector<std::string> arguments = {"-or", "erik"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("The oddRuleName should be erik") {

            REQUIRE(values.oddRuleName  == "erik");

            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(values.evenRuleName == "conway");
            REQUIRE(values.maxGenerations == 100);
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

        }
    }

    GIVEN("The arguments ""-g 10 -f Population_Seed.txt -s 10x10 -er erik -or von_neumann""") {
        std::vector<std::string> arguments = {"-g", "10", "-f", "Population_Seed.txt", "-s", "10x10", 
        "-er", "erik", "-or", "von_neumann"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("Everything should be set correctly") {

            REQUIRE(values.runSimulation == true);
            REQUIRE(values.evenRuleName == "erik");
            REQUIRE(values.oddRuleName  == "von_neumann");
            REQUIRE(values.maxGenerations == 10);
            REQUIRE(fileName == "Population_Seed.txt");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 10);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 10);

        }
        fileName.clear();
        WORLD_DIMENSIONS.WIDTH = 80;
        WORLD_DIMENSIONS.HEIGHT = 24;
    }

}

SCENARIO("Invalid arguments") {
    MainArgumentsParser parser;
    ApplicationValues values;

    GIVEN("A single invalid argument") {
        std::vector<std::string> arguments = {"-q", "göran"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("It should be ignored and the defaults be set") {


            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(values.evenRuleName == "conway");
            REQUIRE(values.oddRuleName  == "conway");
            REQUIRE(values.maxGenerations == 100);
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

        }

    }

    GIVEN("An arguments list which starts with an invalid argument") {
        std::vector<std::string> arguments = {"-q", "göran", "-g", "10", "-er", "erik"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("It should be ignored and the valid argument values be handled correctly") {

            REQUIRE(values.evenRuleName == "erik");
            REQUIRE(values.oddRuleName  == "erik");
            REQUIRE(values.maxGenerations == 10);

            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

        }

    }

    GIVEN("An arguments list which has an invalid argument in the middle") {
        std::vector<std::string> arguments = {"-g", "10", "-q", "göran", "-er", "erik"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("It should be ignored and the valid argument values be handled correctly") {

            REQUIRE(values.evenRuleName == "erik");
            REQUIRE(values.oddRuleName  == "erik");
            REQUIRE(values.maxGenerations == 10);

            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

        }

    }

    GIVEN("An arguments list which ends with an invalid argument") {
        std::vector<std::string> arguments = {"-g", "10", "-er", "erik", "-q", "göran"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("It should be ignored and the valid argument values be handled correctly") {

            REQUIRE(values.evenRuleName == "erik");
            REQUIRE(values.oddRuleName  == "erik");
            REQUIRE(values.maxGenerations == 10);

            //Defaults
            REQUIRE(values.runSimulation == true);
            REQUIRE(fileName == "");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

        }
    }

    GIVEN("An argument without a value") {
        std::vector<std::string> arguments = {"-g"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("The simulation should not be run") {

            REQUIRE(values.runSimulation == false);
        }
    }

    GIVEN("Multiple arguments without a value") {
        std::vector<std::string> arguments = {"-g", "-f", "-s"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);

        values = parser.runParser(argv.data(), argv.size()-1);

        THEN("The simulation should not be run") {

            REQUIRE(values.runSimulation == false);
        }
    }

    GIVEN("A string value for maxGenerations") {
        std::vector<std::string> arguments = {"-g", "abc"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);


        THEN("The program should not crash") {

            values = parser.runParser(argv.data(), argv.size()-1);

        }
    }

    GIVEN("String values for the world-dimensions") {

        std::vector<std::string> arguments = {"-s", "axb"};

        std::vector<char*> argv;
        for (const auto& arg : arguments)
            argv.push_back((char*)arg.data());
        argv.push_back(nullptr);


        THEN("The program should not crash") {

            values = parser.runParser(argv.data(), argv.size()-1);

        }
    }
}
