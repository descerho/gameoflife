#include "catch.hpp"
#include <vector>
#include <algorithm>
#include <GoL_Rules/RuleFactory.h>

TEST_CASE("Only a single object of RuleFactory is allowed and that reference should be returned by getInstance"){
    RuleFactory& ruleFactory1 = RuleFactory::getInstance();
    RuleFactory& ruleFactory2 = RuleFactory::getInstance();
    REQUIRE(&ruleFactory1 == &ruleFactory2);
}

SCENARIO("Crating rules with RuleFactory") {
    GIVEN("Empty cell map and RuleOfExistence is working correctly") {
        RuleFactory &ruleFactory = RuleFactory::getInstance();
        map<Point, Cell>  emptyMap;

        THEN("Creating rules without a string should return default type") {
            REQUIRE((ruleFactory.createAndReturnRule(emptyMap))->getRuleName() == "conway");
        }

        THEN("Creating rules with conway should return that type") {
            REQUIRE((ruleFactory.createAndReturnRule(emptyMap, "conway"))->getRuleName() == "conway");
        }

        THEN("Creating rules with erik should return that type") {
            REQUIRE((ruleFactory.createAndReturnRule(emptyMap, "erik"))->getRuleName() == "erik");
        }

        THEN("Creating rules with von_neumann should return that type") {
            REQUIRE((ruleFactory.createAndReturnRule(emptyMap, "von_neumann"))->getRuleName() == "von_neumann");
        }

        THEN("Creating rules with invalid string should return default type") {
            REQUIRE((ruleFactory.createAndReturnRule(emptyMap, "InvalidString"))->getRuleName() == "conway");
        }
    }
}