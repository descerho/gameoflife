/*
 * Filename    main.cpp
 * Author      Erik Ström
 * Date        October 2017
 * Version     0.1
*/

/** @mainpage
 * @section Introduction
 * This project implements and presents a version of <a href="https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life">Game of Life</a> where a population of cells is spread out in a two-dimensional grid. The cells can either be alive or dead. From the initial population generations of cells is simulated with regards to certain rules. These rules are classically written so that the death, continued life or resurrection(reproduction) depends on how many of its neighbors are alive. This project offers a few different sets of rules, including Conway's classical one and presents the changing generations on screen.
 *
 * @section Usage
 * Run the compiled file with "-h" as argument for up-to-date usage-information
 */


#include <iostream>
#include "GameOfLife.h"
#include "Support/MainArgumentsParser.h"

#ifdef DEBUG
#include <memstat.hpp>
#endif

using namespace std;

int main(int argc, char* argv[]) {

    MainArgumentsParser parser;
    ApplicationValues appValues = parser.runParser(argv, argc);

    if (appValues.runSimulation) {
        // Start simulation
        try {
            GameOfLife gameOfLife = GameOfLife(appValues.maxGenerations, appValues.evenRuleName, appValues.oddRuleName);
            gameOfLife.runSimulation();
        }
        catch(ios_base::failure &e){}

    }

    cout << endl;
    return 0;
}
